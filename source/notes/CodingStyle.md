---
tags: [code]
title: CodingStyle
created: '2020-03-24T16:22:24.434Z'
modified: '2020-04-08T03:40:30.710Z'
---

# CodingStyle

My preferred coding styles, for spinning up new projects:

[[Ruby]]: use [Standard](https://github.com/testdouble/standard), which is built on top of RuboCop.

See [this post](https://evilmartians.com/chronicles/rubocoping-with-legacy-bring-your-ruby-code-up-to-standard) for a good Standard+Rubocop project config, so you can just use Rubocop tooling in your editor.


[[Javascript]]: use [Standard JS](https://standardjs.com/), which is built on top of eslint.

