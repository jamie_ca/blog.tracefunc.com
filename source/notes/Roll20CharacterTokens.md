---
tags: [gaming]
title: Roll20CharacterTokens
created: '2020-04-08T03:37:57.212Z'
modified: '2020-04-08T03:39:55.750Z'
---

# Roll20CharacterTokens

Use [tokenstamp](http://rolladvantage.com/tokenstamp/) to turn character art into decent character icons, with transparency etc.

For consistency in my current campaign:
- Border: row 5, first
- Fade: row 2, first
- Border Tint: RGB 45,145,175
- BG Color: RGB 95,125,135

