---
tags: [commandline]
title: fzf
created: '2020-04-01T23:50:48.677Z'
modified: '2020-04-08T22:35:45.817Z'
---

# fzf

[`fzf`](https://github.com/junegunn/fzf) is an incremental search tool for the terminal.

Here's a [good summary](https://medium.com/@vdeantoni/boost-your-command-line-productivity-with-fuzzy-finder-985aa162ba5d) of setup and usage. Just out of the gate, adding it to zsh and knowing that you can type and hit ctrl+r to match is good enough to get rolling.
